const cors = require('cors');
require('dotenv').config();
var express = require('express');
// var user_file = require('./user.json');
// const user_controller = require('./controllers/user_controllers/user_controller.js');
const user_controller2 = require('./controllers/user_controllers/user_controller2.js');
const auth = require('./controllers/user_controllers/auth.js');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
var port = process.env.PORT || 3000;
const URL_BASE = process.env.URL_BASE

app.use(cors());
app.options('*',cors());
//Peticiòn de GET con conexion a mongo
// GET users consumiendo API REST de mLab
//Peticiòn de GET//Peticion de GET con ID
app.get(URL_BASE + 'users/:id',user_controller2.getUsersId);
app.get(URL_BASE + 'users', user_controller2.getUsers);
//Peticion de GET con ID Accounts
app.get(URL_BASE + 'users/:id/accounts',user_controller2.getAccountsId);
//
app.post(URL_BASE + 'users', user_controller2.postUsers);
//PUT users con parámetro 'id'
app.put(URL_BASE + 'users/:id', user_controller2.putUsers);
// Petición PUT con id de mLab (_id.$oid)
app.put(URL_BASE + 'usersmLab/:id', user_controller2.putUserId);

//Peticion de GET con String
app.get(URL_BASE + 'usersquery',user_controller2.getUsersQuery);

//POST users
//app.post(global.URL_BASE + 'users', user_controller2.setUser);

//PUT users
// app.put(global.URL_BASE + 'users/:id',user_controller2.updateUser);

//DELETE users
app.delete(URL_BASE + 'users/:id',user_controller2.deleteUser);

//LOGIN users
app.post(URL_BASE + 'login', user_controller2.loginUser);

//LOGOUT users
app.post(URL_BASE + 'logout', auth.logoutUser);

//Get de lista de usuarios logueados.
app.get(URL_BASE + 'loggedusers', user_controller2.getOnlineUsers);

//get de lista de usurios logueados
app.get(URL_BASE + 'loggeduser', user_controller2.getOnlineUsersQuery);

app.listen(port, function () {
  console.log('Example app listening on port 3000!');
});
