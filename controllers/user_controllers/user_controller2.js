// require('dotenv').config();
var express = require('express');
var user_file = require('./../../user.json');
var app = express();
var requestJSON = require('request-json');
var baseMLabURL = 'https://api.mlab.com/api/1/databases/techu54db/collections/'
const apikeyMLab = 'apiKey=' + process.env.MLAB_API_KEY;
// app.use(bodyParser.json());

//function getUsers
function getUsers(req, res) {
    res.status(201).send(user_file);
}

//function get users con Query
function getUsersQuery(req, res) {
  console.log('GET con query String');
  console.log(req.query.id);
  console.log(req.query.name);
}

//POST users
function setUser(req, res){
    console.log('POST de users');
    let newID = user_file.length +1;
    let newUser = {
      "id" : newID,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password
    };
    user_file.push(newUser);
    console.log("nuevo usuario: " + newUser);
    res.send(newUser);
}

//PUT users
function updateUser(req, res){
    console.log('PUT de users');
    pos = req.body.id - 1;
    let newUser = {
      "id" : req.params.id,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password
    };
    user_file[pos] = newUser;
    console.log("nuevo usuario: " + JSON.stringify(newUser));
    res.send(newUser);
}

//DELETE users
function deleteUser(req, res){
  var id = req.params.id;
  var queryStringID = 'q={"id":' + id + '}&';
  console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
  var httpClient = requestJSON.createClient(baseMLabURL);
  httpClient.get('user?' +  queryStringID + apikeyMLab,
    function(error, respuestaMLab, body){
      var respuesta = body[0];
      httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
        function(error, respuestaMLab,body){
          res.send(body);
      });
    });
}

//login user
function loginUser(req, res){
    let email = req.body.email, pass=req.body.password;
    let queryString = `q={"email": "${email}","password":"${pass}"}&`;
    let limFilter = 'l=1&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log(queryString);
    httpClient.get('user?' +  queryString + limFilter + apikeyMLab,
      function(error, respuestaMLab, body){
        if(!error){
          if(body.length == 1){
            let login='{"$set":{"logged":true}}';
            let stringQueryPut = `q={"id":${body[0].id}}&`;
            console.log(stringQueryPut);
            httpClient.put('user?'+ stringQueryPut + apikeyMLab, JSON.parse(login),
              function(errPut, resPut,bodyPut){
                res.send({'msg':'Login correcto','user':body[0].email,'userid':body[0].id});
            });
          }
          else {
            // console.log(respuestaMLab)
            response = {"msg" : "Ningún elemento 'user'."};
            res.status(404).send(response);
          }
          }
      });
}

function logoutUser(req, res){
    console.log(req.body.id);
    let i=0, id = req.body.id;
    let encontrado = false, respuesta;
    let buffer = user_file.length;
    while((i< buffer) && !encontrado){
      if(user_file[i].id == id)
      {
        encontrado = true;
        if (user_file[i].logged == true){
          delete user_file[i].logged;
          respuesta = { "mensaje" : "logout correcto", "idUsuario" : i+1 };
          console.log(JSON.stringify(user_file[i]));
        }
        else {
          respuesta = { "mensaje" : "logout incorrecto" };
        }
      }
      i++;
    }
    res.send(respuesta);
}

//Get de lista de usuarios logueados.
function getOnlineUsers(req, res) {
    res.send(user_file.filter(item=>item.logged == true))
}

//get de lista de usurios logueados con query
function getOnlineUsersQuery(req, res) {
    console.log(req.query.limit);
    let limit = req.query.limit;
    let count = 0, i = 0, size = user_file.length, respuesta = [];
    while ( count< limit && i < size ) {
      if (user_file[i].logged == true) {
        respuesta.push(user_file[i]);
        count++;
      }
      i++;
    }
    res.send(respuesta);
}

function getUsers(req, res) {
  console.log("GET /colapi/v3/users");
  var httpClient = requestJSON.createClient(baseMLabURL);
  console.log("Cliente HTTP mLab creado.");
  var queryString = 'f={"_id":0}&';
  httpClient.get('user?' + queryString + apikeyMLab,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo usuario."}
          res.status(500);
      } else {
        if(body.length > 0) {
          console.log(body.length);
          response = body;
        } else {
          response = {"msg" : "Ningún elemento 'user'."}
          res.status(404);
        }
      }
      res.send(response);
    });
}

//function get users con Id
function getUsersId(req, res) {
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    var queryFieldStr = 'q={"id":'+req.params.id+'}&';
    httpClient.get('user?' + queryString + queryFieldStr + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {"msg" : "Error obteniendo usuario."}
            res.status(500);
        } else {
          if(body.length > 0) {
            console.log(body.length);
            console.log("primera llamada");
            response = body;
          } else {
            response = {"msg" : "Ningún elemento 'user'."}
            res.status(404);
          }
        }
        queryFieldStr = 'q={"id":6}&';
        httpClient.get(baseMLabURL+'user?' + queryString + queryFieldStr + apikeyMLab,
          function(err, respuestaMLab, body) {
            var response = {};
            console.log("segunda llamada");
            if(err) {
                response = {"msg" : "Error obteniendo usuario."}
                res.status(500);
            } else {
              if(body.length > 0) {
                console.log(body.length);
                console.log("primera llamada");
                response = body;
              } else {
                response = {"msg" : "Ningún elemento 'user'."}
                res.status(404);
              }
            }
          });
        res.send(response);
      });
}

//function get users con Id
function getAccountsId(req, res) {
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    var queryFieldStr = 'q={"user_id":'+req.params.id+'}&';
    httpClient.get('account?' + queryString + queryFieldStr + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {"msg" : "Error obteniendo usuario."}
            res.status(500);
        } else {
          if(body.length > 0) {
            console.log(body.length);
            response = body;
          } else {
            response = {"msg" : "Ningún elemento 'user'."}
            res.status(404);
          }
        }
        res.send(response);
      });
}

//function get users con Id
function postUsers(req, res) {
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log(req.body);
    console.log(req.body.length);
    httpClient.get('user?' + apikeyMLab,
      function(err, respuestaMLab, body) {
        newID = body.length + 1;
        console.log("newID:" + newID);
        var newUser = {
          "id" : newID,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
        };
        httpClient.post(baseMLabURL + "user?" + apikeyMLab, newUser,
          function(error,respuestaMLab, body){
            console.log(body);
            res.status(201);
            res.send(body);
         }
      );
    });
}

function putUsers(req, res) {
 var id = req.params.id;
 var queryStringID = 'q={"id":' + id + '}&';
 var clienteMlab = requestJSON.createClient(baseMLabURL);
 clienteMlab.get('user?'+ queryStringID + apikeyMLab,
   function(error, respuestaMLab, body) {
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    console.log(req.body);
    console.log(cambio);
    clienteMlab.put(baseMLabURL +'user?' + queryStringID + apikeyMLab, JSON.parse(cambio),
     function(error, respuestaMLab, body) {
       console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
      //res.status(200).send(body);
      res.send(body);
     });
   });
}

function putUserId(req, res) {
  var id = req.params.id;
  let userBody = req.body;
  var queryString = 'q={"id":' + id + '}&';
  var httpClient = requestJSON.createClient(baseMLabURL);
  httpClient.get('user?' + queryString + apikeyMLab,
    function(err, respuestaMLab, body){
      let response = body[0];
      console.log(body);
      //Actualizo campos del usuario
      let updatedUser = {
        "id" : req.body.id,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      };//Otra forma simplificada (para muchas propiedades)
      // var updatedUser = {};
      // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
      // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
      // PUT a mLab
      httpClient.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
        function(err, respuestaMLab, body){
          var response = {};
          if(err) {
              response = {
                "msg" : "Error actualizando usuario."
              }
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "Usuario actualizado correctamente."
              }
              res.status(200);
            }
          }
          res.send(response);
        });
    });
}

module.exports  = {
  getUsers,
  getUsersId,
  getUsersQuery,
  setUser,
  updateUser,
  deleteUser,
  loginUser,
  logoutUser,
  getOnlineUsers,
  getOnlineUsersQuery,
  getAccountsId,
  postUsers,
  putUsers,
  putUserId
}
