var express = require('express');
var user_file = require('./../../user.json');
const global = require('./../../global');
var app = express();
var requestJSON = require('request-json');
var baseMLabURL = 'https://api.mlab.com/api/1/databases/techu54db/collections/'
// app.use(bodyParser.json());

//function getUsers
function getUsers(req, res) {
    res.status(201).send(user_file);
}

//function get users con Id
function getUsersId(req, res) {
    let pos = req.params.id - 1;
    console.log('GET con id = ' + req.params.id);
    let longitud = user_file.length;
    console.log(longitud);
    let respuesta = (user_file[pos] == undefined) ?
      {"msg":"usuario no encontrado"} : user_file[pos];
    res.send(respuesta);
}

//function get users con Query
function getUsersQuery(req, res) {
  console.log('GET con query String');
  console.log(req.query.id);
  console.log(req.query.name);
}

//POST users
function setUser(req, res){
    console.log('POST de users');
    let newID = user_file.length +1;
    let newUser = {
      "id" : newID,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password
    };
    user_file.push(newUser);
    console.log("nuevo usuario: " + newUser);
    res.send(newUser);
}

//PUT users
function updateUser(req, res){
    console.log('PUT de users');
    pos = req.body.id - 1;
    let newUser = {
      "id" : req.params.id,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password
    };
    user_file[pos] = newUser;
    console.log("nuevo usuario: " + JSON.stringify(newUser));
    res.send(newUser);
}

//DELETE users
function deleteUser(req, res){
     console.log("Delete de users ");
     pos = req.params.id - 1;
     if (user_file[pos] == undefined || user_file[pos].id != req.params.id)
        respuesta = {"msg":"usuario no existe"};
     else{
        console.log(JSON.stringify(user_file[pos]));
        user_file.splice(pos,1);
        respuesta = {"msg":"usuario eliminado"};
     }
     res.send(respuesta);
}

//login user
function loginUser(req, res){
    //  console.log("Login de users ");
    //  let item = user_file.find(item=>item.email==req.params.email);
    //  if (item == undefined)
    //     respuesta = {"msg":"usuario no existe"};
    //  else     {
    //     respuesta = JSON.stringify(item);
    //  }
    //  res.send(respuesta);
    console.log(req.body.email);
    console.log(req.body.password);
    let i=0, email = req.body.email, password=req.body.password;
    let encontrado = false;
    while((i< user_file.length) && !encontrado){
      if(user_file[i].email == email && user_file[i].password == password)
      {
        encontrado = true;
        user_file[i].logged=true;
        console.log(JSON.stringify(user_file[i]));
      }
      i++;
    }
    if(encontrado)
      res.send({"encontrado":"sí","id":i});
    else{
      res.send({"encontrado":"no"});
    }
}

function logoutUser(req, res){
    console.log(req.body.id);
    let i=0, id = req.body.id;
    let encontrado = false, respuesta;
    let buffer = user_file.length;
    while((i< buffer) && !encontrado){
      if(user_file[i].id == id)
      {
        encontrado = true;
        if (user_file[i].logged == true){
          delete user_file[i].logged;
          respuesta = { "mensaje" : "logout correcto", "idUsuario" : i+1 };
          console.log(JSON.stringify(user_file[i]));
        }
        else {
          respuesta = { "mensaje" : "logout incorrecto" };
        }
      }
      i++;
    }
    res.send(respuesta);
}

//Get de lista de usuarios logueados.
function getOnlineUsers(req, res) {
    res.send(user_file.filter(item=>item.logged == true))
}

//get de lista de usurios logueados con query
function getOnlineUsersQuery(req, res) {
    console.log(req.query.limit);
    let limit = req.query.limit;
    let count = 0, i = 0, size = user_file.length, respuesta = [];
    while ( count< limit && i < size ) {
      if (user_file[i].logged == true) {
        respuesta.push(user_file[i]);
        count++;
      }
      i++;
    }
    res.send(respuesta);
}

function getMongoUsers(req, res) {
  console.log("GET /colapi/v3/users");
  var httpClient = requestJSON.createClient(baseMLabURL);
  console.log("Cliente HTTP mLab creado.");
  var queryString = 'f={"_id":0}&';
  httpClient.get('user?' + queryString + global.apikeyMLab,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo usuario."}
          res.status(500);
      } else {
        if(body.length > 0) {
          console.log(body.length);
          response = body;
        } else {
          response = {"msg" : "Ningún elemento 'user'."}
          res.status(404);
        }
      }
      res.send(response);
    });
}

module.exports  = {
  getUsers,
  getUsersId,
  getUsersQuery,
  setUser,
  updateUser,
  deleteUser,
  loginUser,
  logoutUser,
  getOnlineUsers,
  getOnlineUsersQuery,
  getMongoUsers
}
