var express = require('express');
var user_file = require('./../../user.json');
// var bodyParser = require('body-parser');
var app = express();
// app.use(bodyParser.json());

//login user
function loginUser(req, res){
    //  console.log("Login de users ");
    //  let item = user_file.find(item=>item.email==req.params.email);
    //  if (item == undefined)
    //     respuesta = {"msg":"usuario no existe"};
    //  else     {
    //     respuesta = JSON.stringify(item);
    //  }
    //  res.send(respuesta);
    console.log(req.body.email);
    console.log(req.body.password);
    let i=0, email = req.body.email, password=req.body.password;
    let encontrado = false;
    while((i< user_file.length) && !encontrado){
      if(user_file[i].email == email && user_file[i].password == password)
      {
        encontrado = true;
        user_file[i].logged=true;
        console.log(JSON.stringify(user_file[i]));
      }
      i++;
    }
    if(encontrado)
      res.send({"encontrado":"sí","id":i});
    else{
      res.send({"encontrado":"no"});
    }
}

function logoutUser(req, res){
    console.log(req.body.id);
    let i=0, id = req.body.id;
    let encontrado = false, respuesta;
    let buffer = user_file.length;
    while((i< buffer) && !encontrado){
      if(user_file[i].id == id)
      {
        encontrado = true;
        if (user_file[i].logged == true){
          delete user_file[i].logged;
          respuesta = { "mensaje" : "logout correcto", "idUsuario" : i+1 };
          console.log(JSON.stringify(user_file[i]));
        }
        else {
          respuesta = { "mensaje" : "logout incorrecto" };
        }
      }
      i++;
    }
    res.send(respuesta);
}

module.exports  = {
  loginUser,
  logoutUser
}
